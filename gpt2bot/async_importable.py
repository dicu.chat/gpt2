# Licensed under the MIT license.

import argparse
import asyncio
import random
import os

from .utils import setup_logger, load_pipeline, clean_text, generate_text, parse_config

logger = setup_logger(__name__)

path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# Script arguments can include path of the config
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument("--config", type=str, default=os.path.join(path, "chatbot.cfg"))
args = arg_parser.parse_args()
# Read the config
config = parse_config(args.config)

pipeline_kwargs = config.get("pipeline", {})
generator_kwargs = config.get("generator", {})
chatbot_kwargs = config.get("chatbot", {})

pipeline = load_pipeline(**pipeline_kwargs)
logger.info("Starting..")

USERS = {
    "turns": {}
}


def get(history, pipeline, selector):
    global generator_kwargs
    resp = generate_text(history, pipeline, **generator_kwargs)
    if len(resp) == 1:
        return resp[0]
    else:
        return selector(resp)


async def get_response(user_id, prompt, turns: list = None):
    # Parse parameters
    max_turns_history = 5
    message_selector = chatbot_kwargs.get("message_selector", random.choice)

    if turns is None:
        turns = USERS["turns"].get(user_id, [])

    USERS["turns"][user_id] = turns

    # A single turn is a group of user messages and bot responses right after
    turns.append(prompt)
    # Merge turns into a single history (don't forget EOS token)
    history = ""
    from_index = max(len(turns) - max_turns_history - 1, 0) if max_turns_history >= 0 else 0
    for turn in turns[from_index:]:
        # Each turn begins with bot messages.
        history += clean_text(turn) + pipeline.tokenizer.eos_token

    # Generate bot messages
    loop = asyncio.get_running_loop()
    bot_message = await loop.run_in_executor(None, get, history, pipeline, message_selector)
    turns.append(bot_message)
    return bot_message, turns

