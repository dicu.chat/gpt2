from sanic.response import json
from sanic import Sanic
import ujson as js
import time
from . import get_response


app = Sanic(name="dicu-ai")


@app.route("/api/get_response", methods=["POST"])
async def api_get_response(request):
    data = js.loads(request.json) if type(request.json) == str else request.json

    prompt = data.get("text") or data.get("prompt")
    if not prompt:
        return json({"status": 400, "message": "You must provide 'prompt' in the request data!"}, status=400)

    resp, turns = await get_response(data["user_id"], prompt, data.get("history"))
    return json({"timestamp": time.monotonic(), "text": resp, "history": turns})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)

