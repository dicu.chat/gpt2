<h1 align="center">
  <br>
  <a href="https://www.morphysm.com/"><img src="./assets/morph_logo_rgb.png" alt="Morphysm" ></a>
  <br>
  <h5 align="center"> Morphysm is a community of engineers, designers and researchers
contributing to security, cryptography, cryptocurrency and AI.</h5>
  <br>
</h1>


<h1 align="center">
  <img src="https://img.shields.io/badge/Python-3.9-red" alt="python badge">

 <img src="https://img.shields.io/badge/docker-20.10-blue" alt="docker badge">
 
 <img src="https://img.shields.io/gitlab/pipeline-status/dicu.chat/server?branch=master" alt="docker build">
</h1> 


Table of Contents
=================

<!--ts-->
   * [Getting Started](#getting-started)
   * [Prerequisites](#prerequisites)
   * [Installation](#usage)
   * [Development](#development)
   * [Demo](#demo)
   * [TroubelShoting](#troubelShoting)
   * [Acknowledgment](#acknowledgment)
   * [Code Owners](#code-owners)
   * [License](#license)
   * [Contact](#contact)
<!--te-->


# 1. Getting Started
This repository contains the gpt2bot model, which is a neural network-powered multi-turn Telegram chatbot. This section is in charge of our AI models.


# 2. Prerequisites

Please make sure that your system has the following prerequisites installed:

- `python 3.9`  
- `docker`
- `docker-compose`


# 3. Installation

1. First get the code by either run this command through SSH:
    ```bash
    git@gitlab.com:dicu.chat/gpt2.git
    ```
    or through HTTPS:
    ```bash
    git clone https://gitlab.com/dicu.chat/gpt2.git
    ```



# 4. Development 
Please follow these steps to run the model locally:


1. Install environment venv :
    ```bash
    virtualenv -p python3.9 .venv && source .venv/bin/activate
    # or, if you don't have 'virtualenv' util installed:
    # python -m venv .venv && source .venv/bin/activate
    ```

2. Install dependencies:
    ```bash
    pip install -r requirements.txt
    ```

3. Start the project
    ```
    python -m gpt2bot
    ```
4. For testing you can run this command and change the message at prompt whatever you like, then you will get response inside the text attribute:
      ```
      curl -X POST -d '{"user_id": "123", "prompt": "Hello, How are you?"}' 0.0.0.0:8080/api/get_response
      ```


# 8.Demo:

Will be updated soon..



# 9. Troubleshooting

If you have encountered any problems while running the code, please open a new issue in this repo and label it bug, and we will assist you in resolving it.

# 10. Acknowledgment:

We want to aknowledge that this repo is based of gpt2bot repo so Many Thanks  [polakowo/gpt2bot](https://github.com/polakowo/gpt2bot) 


# 11. Code Owners

@morphysm/team    :sunglasses:

# 12. License
Our reposiorty is licensed under the terms of the GPLv3 see the license [here!](https://gitlab.com/dicu.chat/gpt2/-/blob/readme/LICENSE2.md)

# 12. Contact

If you'd like to know more about us [Click here!](https://www.morphysm.com/), or You can contact us at "contact@morphysm.com".

