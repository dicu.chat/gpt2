FROM python:3.9-slim-buster

WORKDIR /app/usr/src

COPY requirements.txt .
RUN pip3 install --no-cache-dir -U pip wheel setuptools \
 && pip3 install --no-cache-dir -r requirements.txt

COPY gpt2bot gpt2bot
COPY chatbot.cfg chatbot.cfg
CMD python -m gpt2bot
